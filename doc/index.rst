=====================================
Welcome to toto2titi's documentation!
=====================================

`toto2titi` is a rag-bag of simple, desktop, utilities.

.. toctree::
   :maxdepth: 1
   :glob:

   paste2file
   paste2qrcode
   paste2sms
   wifi2qrcode
   faq
   toto2titi/index

Download and install
====================

See the `main project page <http://git.framasoft.org/spalax/paste2sms>`_ for
instructions, and `changelog
<https://git.framasoft.org/spalax/paste2sms/blob/main/CHANGELOG.md>`_.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
