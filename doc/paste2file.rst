=================================================
`paste2file` 💾 Paste clipboard content as a file
=================================================

You just copied an interesting code snippet (or whatever) on the web, and you want to open an editor, paste it, and save it? This small applications lets you choose your filename, and saves the clipboard content into it.

Edition
=======

If an ``editor`` option is set in the ``general`` option of the :ref:`configuration file <configfile>`, once the file is saved, it is opened with the editor.

Binaries
========

The `paste2file` binary does not take any interesting arguments.
