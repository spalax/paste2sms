=======================================================
`wifi2qrcode` 🌐 Display wifi credentials as a QR code.
=======================================================

Scanning this code with your smartphone will automatically connect it to this network.

.. contents::
   :local:
   :depth: 2

Rationale
=========

A friend is visiting you, and wants to connect her smartphone to your wifi network. Simply run this application, and her phone should automatically connect to your wifi when scanning this QR code.

Binaries
========

The `wifi2qrcode` binary does not take any interesting arguments.
