========================================================
`paste2qrcode` 🔳 Display clipboard content as a QR code
========================================================

Display clipboard content as a QR code (or several QR codes), to send it to a smartphone.

Binaries
========

The `paste2qrcode` binary does not take any interesting arguments.
